# Arhitektura informacionog sistema SystemPro

## Uvod
Osnovne karakteristike sistema koje su donete tokom projektovanja arhitekture su:
- Tip aplikacije: Web aplikacija, desktop deo aplikacije za administratora
- Razvojne tehnologije: AngularJS, Django, QT, C++
- Strategije isporučivanja: jedan serverski računar
- Prateće komponente
    - Logovanje na sistem
    - Backup baze
        Podsistem koji čuva kopije baze čime se sprečava gubljenje podataka
    - Čuvanje logova
        Čuvaju se podaci o tome koji korisnici su kada koristili aplikaciju i koje su akcije izvršavali.
        


## Tip arhitekture

Arhitektura sistema je nalik na klijent-server arhitekturu, gde je svaki entitet baziran na četvoroslojnoj arhitekturi. Na klijentskoj strani se nalaze naredni slojevi:
- Prezentacioni sloj (sloj korisničkog interfejsa):
    Ovo je najviši nivo aplikacije. Glavna funkcija ovog sloja je da prevede zadatke i rezultate u nešto što korisnik moze lako da razume.
- Klijentski kontroler (servisni sloj)
    Logički sloj koji kontroliše funkcionalnost aplikacije putem detaljnog procesiranja, vrši se na klijentskoj strani, tako da sve osetljive operacije se moraju dodatno obraditi na narednom sloju.
- Serverski kontroler
    Logički sloj, koji ima sličnu svrhu kao i klijentski kontroler, sa tim sto je obezbedjeno da klijenti nemaju pristupa ovom delu aplikacije stoga možemo vršiti detaljne autorizacije i validacije podataka. Ovde takodje vršimo komunikaciju sa bazom.
- Sloj podataka
    Ukljucuje mehanizme za bezbedan i konzistentan pristup podacima, u našem slučaju bazu podataka. Sloj podataka enkapsulira ove mehanizme i daje lak pristup podacima. Zadatak ovog sloja je da pruži pristup sloju iznad da koristi podatke na jednostavan i uniforman način.
    



