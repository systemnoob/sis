#ifndef PARENTS_H
#define PARENTS_H

#include <QWidget>
#include <QVector>
#include <QRadioButton>
#include <fstream>
#include <iostream>
#include <QBoxLayout>

class MainWindow;

namespace Ui {
class Parents;
}

class Parents : public QWidget
{
    Q_OBJECT

public:
    explicit Parents(QWidget *parent = 0);
    void setMainWindow(MainWindow *mainWin);
    void setParents();
    int findParentID();
    void uncheckButtons();
    int numberOfSelected();
    ~Parents();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Parents *ui;
    MainWindow *mw;
    std::string fileParents = "parents.txt";
    std::map<std::string,int> parentsMap;
    QVector <QRadioButton *> listOfRadioButtons;
    QWidget *container;
    QVBoxLayout* containerLayout;
    int parentID;
};

#endif // PARENTS_H
