#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QString>
#include <QMap>
#include <fstream>
#include <iostream>
#include <QDebug>
#include <QWidget>
#include <QVBoxLayout>
#include <QCheckBox>
#include <sstream>
#include <QTextBrowser>

class Payment;
class Parents;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Payment *pay, QWidget *parent = 0);
    void ReadTransactions(int userID);
    void setUserID(int ID);
    bool findChosenTransactions();
    void deleteChosenTransactions();
    void uncheckChosenTransactions();
    void removeTransactionsFromMap(std::string);
    int findUserCreditCardNumber();
    void setParents(Parents *p);
    void insertFinishedTransactions(std::string);
    void setFinishedTransactions(int);
    void clearLabel();
    ~MainWindow();
    const std::string fileTransactions = "transactions.txt";
    const std::string fileCreditCards = "creditCards.txt";
    const std::string fileFinishedTransactions = "finishedTransactions.txt";

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    std::map<std::string,double> transactions;
    QVector<QCheckBox *> listOfCheckBoxes;
    int bill;
    int userID;
    Payment *payment;
    QWidget *container;
    QVBoxLayout* containerLayout;
    Parents *parents;
    QTextBrowser *browser;

};

#endif // MAINWINDOW_H
