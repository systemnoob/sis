#include <QApplication>
#include "mainwindow.h"
#include "login.h"
#include "payment.h"
#include "parents.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Parents p;
    Payment pay;
    MainWindow w(&pay);
    Login l(&p,&w);
    l.show();

    return a.exec();
}
