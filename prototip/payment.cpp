#include "payment.h"
#include "ui_payment.h"
#include "mainwindow.h"

Payment::Payment(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Payment)
{
    ui->setupUi(this);
    ui->label_4->setStyleSheet("QLabel {color : red; }");
}

Payment::~Payment()
{
    delete ui;
}

void Payment::addTransaction (std::string transaction)
{
    ui->textBrowser->append(QString::fromStdString(transaction));
    ui->textBrowser->append("\n");
}

void Payment::setBill(int bill)
{
    ui->lineEdit->setEnabled(false);
    ui->textBrowser->setEnabled(false);
    ui->lineEdit_2->setEnabled(false);
    ui->lineEdit->setText(QString::number(bill));
}

void Payment::on_pushButton_clicked()
{
    std::ifstream file (mw->fileCreditCards);

    std::string line;

    int pos, pos2;

    while (getline(file, line)){
        pos = line.find_first_of(" ");
        pos2 = line.find_last_of(" ");
        if(std::stoi(line.substr(pos+1, pos2)) == std::stoi(ui->lineEdit_2->text().toStdString())) {
            if (std::stoi(line.substr(pos2+1)) < std::stoi(ui->lineEdit->text().toStdString())) {
                ui->label_4->setText("Na kartici nema dovoljno novca!");
                return ;
            }
            else {
                file.close();
                updateMoney(line);
                break;
            }
        }
    }
    backToMainWindowAfterSuccess();
}

void Payment::setUserID(int ID)
{
    userID = ID;
}

void Payment::setMainWin(MainWindow *mainWin)
{
    mw = mainWin;
}

void Payment::addTransactionToVector (std::string str)
{
    int pos = str.find_last_of('(');
    transactions.append(str.substr(0, pos-1));
}

void Payment::deleteFromFile ()
{
    bool found = false;
    std::ifstream file (mw->fileTransactions);
    std::ofstream tmp;
    tmp.open("tmp.txt");
    std::string line;
    while (getline(file, line)){
        if (line[0] == '#' && userID == std::stoi(line.substr(2,line.size()))) {
            tmp << line << std::endl;
            found = true;
            break;
        }
        tmp << line << std::endl;
    }

    std::string line2;

    if (found) {
        while (getline(file, line)){
            if (line[0] == '#'){
                tmp << line << std::endl;
                break;
            }
            if (transactions.indexOf(line) != -1) {
                getline(file, line2);
            }
            else{
                tmp << line << std::endl;
                getline(file, line2);
                tmp << line2 << std::endl;
            }
        }
    }
    while(getline(file,line)){
        tmp << line << std::endl;
    }
    tmp.close();
    file.close();
    remove(mw->fileTransactions.c_str());
    rename("tmp.txt", mw->fileTransactions.c_str());
}

void Payment::on_pushButton_2_clicked()
{
    mw->uncheckChosenTransactions();
    clearAll();
    mw->clearLabel();
    this->hide();
    mw->show();
}

void Payment::setCreditCardNumber(int amount)
{
    ui->lineEdit_2->setText(QString::number(amount));
}

void Payment::clearAll()
{
    transactions.clear();
    ui->textBrowser->clear();
    ui->lineEdit_2->clear();
    ui->label_4->clear();
}

void Payment::on_pushButton_3_clicked()
{
    QMessageBox::StandardButton reply;

    reply = QMessageBox::question(this, "Obavestenje", "Da li ste sigurni da korisnik placa gotovinom?", QMessageBox::Yes|QMessageBox::No);

    if (reply == QMessageBox::Yes) {
        backToMainWindowAfterSuccess();
    }
}

void Payment::backToMainWindowAfterSuccess()
{
    deleteFromFile();
    deleteFromFinishedFile();

    for ( std::string str : transactions ){
        mw->insertFinishedTransactions(str);
        mw->removeTransactionsFromMap(str);
    }

    mw->deleteChosenTransactions();
    clearAll();
    mw->clearLabel();
    this->hide();
    mw->show();
}

void Payment::updateMoney(std::string str)
{
    std::ifstream file;
    file.open(mw->fileCreditCards);

    std::ofstream tmp;
    tmp.open("tmp.txt");
    std::string line;
    while (std::getline(file, line)){
        if (!line.compare(str)) {
            tmp << line.substr(0,line.find_last_of(" ")) << " ";
            tmp << std::stoi(line.substr(line.find_last_of(" ")+1)) - std::stoi(ui->lineEdit->text().toStdString()) << std::endl;
        }
        else
            tmp << line << std::endl;
    }
    tmp.close();
    file.close();
    remove(mw->fileCreditCards.c_str());
    rename("tmp.txt", mw->fileCreditCards.c_str());
}

void Payment::deleteFromFinishedFile()
{
    bool found = false;
    std::ifstream file (mw->fileFinishedTransactions);
    std::ofstream tmp;
    tmp.open("tmp.txt");
    std::string line;

    while (getline(file, line)){
        if (line[0] == '#' && userID == std::stoi(line.substr(2,line.size()))) {
            tmp << line << std::endl;
            found = true;
            break;
        }
        tmp << line << std::endl;
    }

    if (found) {
        for (auto t : transactions)
            tmp << t << std::endl;
    }
    else
        return ;

    while(getline(file,line)){
        tmp << line << std::endl;
    }

    tmp.close();
    file.close();
    remove(mw->fileFinishedTransactions.c_str());
    rename("tmp.txt", mw->fileFinishedTransactions.c_str());
}
