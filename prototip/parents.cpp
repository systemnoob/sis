#include "parents.h"
#include "mainwindow.h"
#include "ui_parents.h"

Parents::Parents(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Parents)
{
    ui->setupUi(this);
    ui->label_2->setStyleSheet("QLabel {color : red};");
}

Parents::~Parents()
{
    delete ui;
    for (auto b : listOfRadioButtons)
        delete b;
}

void Parents::on_pushButton_clicked()
{
    parentID = findParentID();

    if (parentID == -1) {
        ui->label_2->setText("Morate odabrati jednog korisnika!\n");
        return ;
    }
    else if (numberOfSelected() > 1) {
        ui->label_2->setText("Ne mozete odabrati vise od jednog korisnika!");
        return ;
    }
    mw->setUserID(parentID);
    mw->ReadTransactions(parentID);
    mw->setFinishedTransactions(parentID);
    mw->setParents(this);
    mw->clearLabel();
    this->hide();
    mw->show();
    uncheckButtons();
}

void Parents::setMainWindow(MainWindow *mainWin)
{
    mw = mainWin;
}

void Parents::setParents()
{
    std::ifstream file (fileParents);

    int userID, pos;

    std::string line;

    while (getline(file, line)){
        pos = line.find(" ");
        userID = std::stoi(line.substr(0,pos));
        parentsMap.emplace(std::make_pair(line.substr(pos+1), userID));
    }

    container = new QWidget();
    containerLayout = new QVBoxLayout();
    container->setLayout(containerLayout);
    ui->scrollArea->setWidget(container);
    for (auto p : parentsMap) {
        line = "";
        line.append(p.first);
        QRadioButton *radioButton = new QRadioButton(QString::fromStdString(line));
        containerLayout->addWidget(radioButton);
        listOfRadioButtons.append(radioButton);
    }
    file.close();
}

int Parents::findParentID()
{
    for (QRadioButton *box : listOfRadioButtons) {
        if (box->isChecked()) {
            return parentsMap[box->text().toStdString()];
        }
    }

    return -1;
}

void Parents::uncheckButtons()
{
    ui->label_2->clear();
    for (QRadioButton *b : listOfRadioButtons) {
        b->setAutoExclusive(false);
        b->setChecked(false);
        b->setAutoExclusive(true);
    }
}

int Parents::numberOfSelected() {
    int ret = 0;

    for (QRadioButton *b : listOfRadioButtons)
        if (b->isChecked())
            ret++;

    return ret;
}
