#include "login.h"
#include "ui_login.h"
#include "mainwindow.h"
#include "parents.h"

Login::Login(Parents *parents, MainWindow *mainWin, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Login),
    parents(parents),
    mw(mainWin)
{
    ui->setupUi(this);
    ui->label->setStyleSheet("QLabel {color : red; }");
}

Login::~Login()
{
    delete ui;
}

void Login::on_pushButton_clicked()
{
    if (!ui->lineEdit->text().compare("") || !ui->lineEdit_2->text().compare("")) {
        ui->label->setText("Morate popuniti polja korisnicko ime i lozinka!");
        return ;
    }

    std::ifstream file (fileAccounts);

    std::string line;

    bool found = false;

    while (getline(file, line)){
        if(checkAccount(line)) {
            found = true;
            break;
        }
    }

    if (!found) {
        ui->label->setText("Pogresno korisnicko ime ili lozinka!");
        return ;
    }
    file.close();
    this->hide();
    parents->setMainWindow(mw);
    parents->setParents();
    parents->show();
}

bool Login::checkAccount(std::string account)
{
    int pos = account.find(" ");
    int pos2 = account.substr(pos+1).find(" ");
    if (!account.substr(0,pos).compare(ui->lineEdit->text().toStdString())) {
        if (!account.substr(pos+1, pos2).compare(ui->lineEdit_2->text().toStdString())) {
            return true;
        }
    }

    return false;
}
