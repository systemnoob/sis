#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "parents.h"
#include "payment.h"
#include <QScrollBar>

MainWindow::MainWindow(Payment *pay, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    payment(pay)
{
    container = new QWidget();
    containerLayout = new QVBoxLayout();
    ui->setupUi(this);
    ui->label_2->setStyleSheet("QLabel {color : red};");
    bill = 0;
    browser = new QTextBrowser();
    ui->scrollArea->setMaximumHeight(80);
//    ui->scrollArea_2->setMaximumHeight(80);
    ui->textBrowser->setMaximumSize(382,80);
    container->setLayout(containerLayout);
    ui->scrollArea->setWidget(container);
//    ui->scrollArea_2->setWidget(browser);
}

MainWindow::~MainWindow()
{
    delete ui;

    for (auto b : listOfCheckBoxes)
        delete b;
}

void MainWindow::setUserID(int ID)
{
    userID = ID;
}

void MainWindow::ReadTransactions(int userID)
{
    bool found = false;

    std::ifstream file (fileTransactions);

    std::string line;

    while (getline(file, line)){
        if (line[0] == '#' && userID == std::stoi(line.substr(2,line.size()))) {
            found = true;
            break;
        }
    }

    std::string line2;
    double price;

    if (found) {
        while (getline(file, line)){
            if (line[0] == '#')
                   break;

            getline(file, line2);
            price = std::stod(line2);

            transactions.emplace(std::make_pair(line,price));
        }
    }
    else
        return ;

    for (auto t : transactions) {
        line = "";
        line.append(t.first);
        line.append(" ");
        line.append("(");
        std::ostringstream strs;
        strs << t.second;
        line2 = strs.str();
        line.append(line2);
        line.append(" rsd)");
        QCheckBox *checkbox = new QCheckBox(QString::fromStdString(line));
        containerLayout->addWidget(checkbox);
        listOfCheckBoxes.append(checkbox);
    }
    file.close(); //TODO jel ovo treba?
}

void MainWindow::on_pushButton_clicked()
{
    bill = 0;
    bool isEmpty = findChosenTransactions();
    if (isEmpty){
        ui->label_2->setText("Morate izabrati neku transakciju!");
        return;
    }
    this->hide();
    payment->setBill(bill);
    payment->setUserID(userID);
    payment->setCreditCardNumber(findUserCreditCardNumber());
    payment->show();
    payment->setMainWin(this);
}

bool MainWindow::findChosenTransactions()
{
    int pos;
    bool isEmpty = true;
    for (QCheckBox *box : listOfCheckBoxes) {
        if (box->isChecked()) {
            pos = box->text().toStdString().find("(");
            bill += transactions[box->text().toStdString().substr(0,pos-1)];
            payment->addTransaction(box->text().toStdString());
            payment->addTransactionToVector(box->text().toStdString());
            isEmpty = false;
        }
    }
    return isEmpty;
}


void MainWindow::deleteChosenTransactions()
{
    QCheckBox *box;
    for ( int i = 0; i < listOfCheckBoxes.size(); i++){
        box = listOfCheckBoxes[i];
        if (box->isChecked()) {
            delete box;
            listOfCheckBoxes.removeAt(i);
            i--;
        }
    }
}


void MainWindow::uncheckChosenTransactions()
{
    for (QCheckBox *box : listOfCheckBoxes) {
        if (box->isChecked()) {
            box->setChecked(false);
        }
    }
}

void MainWindow::removeTransactionsFromMap(std::string str)
{
    transactions.erase(transactions.find(str));
}

int MainWindow::findUserCreditCardNumber()
{
    std::ifstream file (fileCreditCards);

    std::string line;

    int pos, pos2;
    while (getline(file, line)){
        pos = line.find_first_of(" ");
        pos2 = line.find_last_of(" ");
        if(std::stoi(line.substr(0,pos)) == userID) {
            file.close();
            return std::stoi(line.substr(pos+1,pos2));
        }
    }

    file.close();
    return -1;
}

void MainWindow::on_pushButton_2_clicked()
{
    for (QCheckBox *box : listOfCheckBoxes)
        delete box;

    listOfCheckBoxes.clear();

    for (auto t : transactions)
        removeTransactionsFromMap(t.first);

    this->hide();
    parents->uncheckButtons();
    parents->show();
}

void MainWindow::setParents(Parents *p)
{
    parents = p;
}

void MainWindow::insertFinishedTransactions(std::string str)
{
    ui->textBrowser->append(QString::fromStdString(str));
    ui->textBrowser->append("\n");
    ui->textBrowser->moveCursor(QTextCursor::Start);
    ui->textBrowser->ensureCursorVisible();
}

void MainWindow::setFinishedTransactions(int userID)
{
    ui->textBrowser->clear();

    bool found = false;

    std::ifstream file (fileFinishedTransactions);

    std::string line;

    while (getline(file, line)){
        if (line[0] == '#' && userID == std::stoi(line.substr(2,line.size()))) {
            found = true;
            break;
        }
    }

    if (found) {
        while (getline(file, line)){
            if (line[0] == '#')
                   break;

            ui->textBrowser->append(QString::fromStdString(line));
            ui->textBrowser->append("\n");
        }
    }
    else
        return ;

    file.close();
    ui->textBrowser->moveCursor(QTextCursor::Start);
    ui->textBrowser->ensureCursorVisible();

}

void MainWindow::clearLabel()
{
    ui->label_2->clear();
}
