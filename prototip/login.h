#ifndef LOGIN_H
#define LOGIN_H

#include <QDebug>
#include <QWidget>
#include <fstream>
#include <iostream>

class Parents;
class MainWindow;

namespace Ui {
class Login;
}

class Login : public QWidget
{
    Q_OBJECT

public:
    explicit Login(Parents *p, MainWindow *mainWin, QWidget *parent = 0);
    bool checkAccount(std::string account);
    void setWindow(MainWindow *win);
    ~Login();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Login *ui;
    Parents *parents;
    MainWindow *mw;
    std::string fileAccounts = "accounts.txt";
    int userID;
};

#endif // LOGIN_H
