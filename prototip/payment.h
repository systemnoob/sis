#ifndef PAYMENT_H
#define PAYMENT_H

#include <QWidget>
#include <QMessageBox>
#include <QVector>
#include <fstream>
#include <iostream>

class MainWindow;

namespace Ui {
class Payment;
}

class Payment : public QWidget
{
    Q_OBJECT

public:
    explicit Payment(QWidget *parent = 0);
    void addTransaction (std::string);
    void addTransactionToVector (std::string str);
    void setBill(int bill);
    void setUserID(int ID);
    void setMainWin(MainWindow *mainWin);
    void deleteFromFile();
    void deleteFromFinishedFile();
    void setCreditCardNumber(int amount);
    void clearAll();
    void backToMainWindowAfterSuccess();
    void updateMoney(std::string str);
    ~Payment();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    MainWindow *mw;
    Ui::Payment *ui;
    int userID;
    QVector<std::string> transactions;
};

#endif // PAYMENT_H
