# ------------------------------------------------------------------------------------------------------------------------
# Testiranje
# ------------------------------------------------------------------------------------------------------------------------
Naziv: Elektronska prijava ucenika za testiranje
Kratak opis: Roditelj prijavljjuje dete za polaganje testa preko Veb stranice na kojoj
popunjava formular.
Akter: Roditelj
Preduslovi: Prijave su otvorene, ucenik je najmanje 3. razred osnovne skole, a najvise 8. razred.
Postuslovi: Kandidat je prijavljen
Glavni tok:
    1. Roditelj odlazi na vebsajt i otvara formu za prijavu
    2. Roditelj popunjava formu za prijavu
    3. Sistem vrsi validaciju prijave.
    4. Sistem pamti unetu prijavu
    5. Sistem obavestava roditelja o uspesnoj/neuspesnoj prijavi
Alternativni tokovi:
    3. Nevalidna prijava:
        3.1. Vec prijavljen, slucaj upotrebe se zavrsava.

Naziv: Prijavljivanje ucenika za testiranje od strane roditelja u skoli
Kratak opis: Roditelj prijavljuje dete za polaganje testa licno.
             Administrator unosi podatke o prijavi.
Akteri: Roditelj, Administrator.
Preduslovi: Prijave su u toku. Roditelj donosi djacku knjizicu ucenika.
Postuslovi: Svi kandidati su prijavljeni.
Glavni tok:
    1. Roditelj dobija formular za prijavu od administratora
    2. Roditelj popunjava formular za prijavu.
    3. Administrator zabelezava prijavu u bazu.
Alternativni tokovi:
    1. Roditelju fali djacka knjizica, slucaj upotrebe se zavrsava.

Naziv: Evaluacija znanja
Kratak opis: Kandidat polaze test. Nastavnici pregledaju. Administrator unosi rezultate i sistem ih rangira.
Ucesnici: ucenik, nastavnik, administrator.
Preduslovi:
    1. Kandidat je prijavljen.
    2. Obezbednjeno je mesto za polaganje testa.
Postuslovi:
    1. Kandidati su polagali test.
    2. Testovi su pregledani.
    3. Rezultati su objavljeni.
Glavni tok:
    1. Ucenik polaze test.
    2. Nastavnik pregleda test.
    3. Nastavnik unosi rezultate u sistem
    3. Sistem rangira kandidate po broju bodova.
    4. Administrator objavljuje rezultate.
Alternativni tokovi:
    1. Ucenik nije izasao na test, u tom slucaju pri
    unosu poena mu se upisuje 0, slucaj upotrebe se zavrsava

# ------------------------------------------------------------------------------------------------------------------------
# Upis godine
# ------------------------------------------------------------------------------------------------------------------------
Naziv: Upis godine
Kratak opis: Roditelj uplacuje skolarinu za ucenika koju administrator evidentira.
Ucesnici: Roditelj, administrator.
Preduslovi: Ucenik polozio prijemni.
Postuslovi: Ucenik biva upisan u skolu, a roditelj ima potvrdu o uplati.
Glavni tok:
    1. Roditelj uplacuje prvu mesecnu ratu skolarine licno ili elektronskim putem.
    2. Administrator unosi u sistem uplatu koju je roditelj izvrsio.
       Koristi se slucaj upotrebe "Kreiranje naloga".
    3. Administrator omogucuje roditelju (pristupom delu za placanje roditeljskog account-a) da vidi da je uplata uspesno izvrsena.
    4. Administator unosi u bazu podatke o tome da je ucenik upisan.
       Koristi se slucaj upotrebe "Kreiranje naloga".
Alternativni tok:
    1. Roditelj odustao od uplate, odnosno povukao je, ucenik ne pohadja skolu. Skolarina se vraca ukoliko je vec uplacena, narednih opisanih koraka nema.
    3. Account nije kreiran, treba ga kreirati.
    4. Ukoliko ucenika nema u bazi treba ga dodati, ili ako nije kreiran account potrebno je to uraditi.

Naziv: Rasporedjivanje ucenika po grupama
Kratak opis: Administrator rasporedjuje ucenike po grupama
Ucesnici: Administrator
Preduslovi: Ucenici su upisani u skolu
Postuslovi: Ucenici su rasporedjeni po grupama
Glavni tok:
    1. Administrator otvara formu za rasporedjivanje ucenika
    2. Administrator pokrece automatsko rasporedjivanje ucenika po njihovim godinama
Alternativni tokovi:
    2. Administrator vrsi potrebne izmene u generisanom rasporedu

# ------------------------------------------------------------------------------------------------------------------------
# Nastava
# ------------------------------------------------------------------------------------------------------------------------
Naziv: Upisivanje rezultata zavrsnog testa
Kratak opis: Profesor unosi poene ucenika koje je osvojio na zavrsnom testu
Ucesnici: Profesor
Preduslovi: Ucenik je polagao zavrsni ispit
Postuslovi: Uneti su mu bodovi
Glavni tok:
    1. Profesor otvara formu za unos bodova
    2. Profesor unosi bodove za ucenika
    3. Profesor zakljucuje unos poena
Alternativni tok
    2. Ucenik nije ostvario minimalan broj poena za prolaz, profesor unosi bodove i kroz formu oznacava da test nije polozen

Naziv: Pravljenje rasporeda casova
Kratak opis: Administrator i nastavnici kreiraju raspored casova za tekucu godinu
Ucesnici: Administrator, nastavnik
Preduslovi: Ucenici upisani
Postuslovi: Raspored casova koji ce se primenjivati tokom skolske godine
Glavni tok:
    1. Nastavnicima se dodeljuju predmeti koje ce drzati tekuce godine
    2. Nastavnik i administrator se dogovaraju i sastavljaju raspored casova koji ce odgovarati nastavnicima.
    3. Administrator unosi raspored casova u sistem
    4. Administrator omogucava relevantnim osobama pristup rasporedu casova (ucenici, profesori, roditelji)

Naziv: Pravljenje rasporeda konsultacija
Kratak opis: Administrator i nastavnici kreiraju raspored konsultacija za tekucu skolsku godinu.
Ucesnici: Administrator, nastavnik
Preduslovi: Ucenici upisani
Postuslovi: Raspored casova koji ce se primenjivati tokom skolske godine
Glavni tok:
    1. Nastavnici iznose zeljene termine konsultacija
    2. Administrator rasporedjuje nastavnike

Naziv: Razmena poruka
Kratak opis: Profesori, ucenici i roditelji mogu da razmenjuju poruke
Ucesnici: Profesor, roditelj, ucenik
Preduslovi: Subjekti ulogovani na sistem
Postuslovi: Razmenjene poruke izmedju subjekata
Glavni tok:
    1. Ulogovani korisnik odlazi na stranu za slanje poruka.
    2. Korisnik bira nalog drugog korisnika kome zeli da posalje poruku.
    3. Korisnik kuca poruku i salje je.
    4. Sistem cuva informacije o poruci.
    5. Sistem obavestava primaoca o pristigloj poruci
Alternativni tokovi:
    3. Korisnik odustaje od slanja poruke, u tom slucaju sistem cuva informaciju o poruci(drafts). Slucaj opetrebe se zavrsava.


# ------------------------------------------------------------------------------------------------------------------------
# Upravljanje podacima o korisnicima
# ------------------------------------------------------------------------------------------------------------------------
gaziv: Izmena niskorizicnih informacija
Kratak opis: Nastavnik, ucenik ili roditelj (u dalje tekstu korisnik) menjaju licne informacije
Akter: Nastavnik, ucenik ili roditelj
Preduslovi: Korisnik ima nalog i prijavljen je na njega
Postuslovi: Informacije su izmenjene u celom sistemu
Glavni tok:
    1. Korisnik pristupa stranici za izmenu podataka
    2. Korisnik menja podatke
    3. Korisnik potvrdjuje promenu
    4. Sistem unosi promenu u bazu
Alternativni tokovi:
    3. Nevalidan podatak:
        Ukoliko podatak ne odgovara uslovima (prekoracena duzina polja),
        korisniku se vraca informacija sta nije dobro uneto.

Naziv: Izmena visokorizicnih informacija
Kratak opis: Administrator menja informacije na zahtev nastavnika, ucenika ili
             roditelja. To ukljucuje izmenu sifri i slucaj da su pogresno povezani
             nalozi roditelja i ucenika.
Akter: Administrator
Preduslovi:
    1. Nastavnik, ucenik ili roditelj zahteva promenu nekog podatka
    2. Administrator je ulogovan u sistem
Postuslovi: Informacije su izmenjene u celom sistemu
Glavni tok:
    1. Administrator pristupa stranici za izmenu podataka
    2. Administrator menja podatak
    3. Administrator potvrdjuje promenu
    4. Sistem unosi promenu u bazu
Alternativni tokovi:
    3. Nevalidan podatak:
        Ukoliko podatak ne odgovara uslovima (prekoracena duzina polja),
        administratoru se vraca informacija sta nije dobro uneto. Slucaj upotrebe se nastavlja od 2.

Naziv: Pravljenje naloga
Kratak opis: Administrator pravi naloge za ucenike, profesore i roditelje
Ucesnici: Administrator
Preduslovi: Ucenik se upisuje u prvu godinu ili se nov profesor zaposljava
Postuslovi: Napravljen je nalog za odgovarajuceg korisnika
Glavni tok:
    1. Administrator otvara formu za kreiranje novog naloga.
    2. Administrator unosi relevantne podatke (ime, prezime, datum rodjenja, slika,
       tip korisnika (ucenik/roditelj/profesor)).
    3. Administrator pravi odgovarajucu lozinku (jmbg).
    4. Administrator unosi u bazu nalog novog korisnika.
Alternativni tok:
    2. Administrator nema potrebne podatke o korisniku ciji nalog zeli da napravi, slucaj upotrebe se zavrsava.


# ------------------------------------------------------------------------------------------------------------------------
# Finansije
# ------------------------------------------------------------------------------------------------------------------------
Naziv: Isplata nastavnika
Kratak opis: Sistem obradjuje broj održanih časova nastavnika.
             Sistem salje zahtev za uplatu na račun nastavnika. Administrator potvrdjuje uplatu.
Akteri: Administrator
Preduslovi: Nastavnik je prethodnog meseca držao casove, Prvi je dan u mesecu
Postuslovi: Nastavniku je isplacena mesecna plata
Glavni tok:
    1. Administrator zahteva obradu svih nastavnika prvog dana u mesecu.
    2. Sistem u zavisnosti od broja casova koje je nastavnik drzao prethodnog meseca salje zahtev za uplatu odgovarajuce svote novca na racun nastavnika.
    3. Administrator potvrdjuje automatski generisanu uplatu

Naziv: Elektronsko placanje mesecne skolarine
Kratak opis: Roditelj placa mesecni deo skolarine elektronskim putem
Akteri: Roditelj
Preduslovi: Postoji neplacena taksa, roditelj ima registrovanu karticu
Postuslovi: Uplata je izvrsena i evidentirana
Glavni tok:
    1. Sistem prikazuje listu neizmirenih obaveza
    2. Roditelj bira koju neizmirenu obavezu zeli da regulise
    3. Vrsi elektronsku uplatu preko kartice
    4. Sistem evidentira uplatu
    5. Sistem azurira pregled finansijskih obaveza
Alternativni tokovi:
    2. Na kartici nema dovoljno novca, uplata se ne moze izvrsiti, slucaj upotrebe se nastavlja od 1.

Naziv: Licno placanje mesecne skolarine
Kratak opis: Roditelj placa mesecni deo skolarine licno
Akteri: Roditelj, Administrator
Preduslovi: Postoji neplacena taksa
Postuslovi: Uplata je izvrsena i evidentirana
Glavni tok:
    1. Sistem prikazuje administratoru listu neizmirenih obaveza
    2. Roditelj bira koju neizmirenu obavezu zeli da regulise
    3. Roditelj daje odgovarajucu svotu novca administratoru
    4. Administrator evidentira i zakljucuje uplatu
    5. Administrator izdaje uplatnicu roditelju
    6. Sistem azurira pregled finansijskih obaveza
