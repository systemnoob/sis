# Beleske

### Analiza sistema
- Koje probleme resavate
- Kako resavate
- Kako utice na aktere sistema
- Ceo doprinos
- Ukoliko postoji vise nezavisnih delova sistema (module), mogu se opisati odvojeno

### Da sam ucenik voleo bih
- Da vidim raspored
- Skripte
- Spisak predmeta (kategorije)
- Videti obavestenja
- Spisak zadataka za vezbu
- Reference kao online okruzenja za ucenje
- Konsultacije (kad su i ko drzi)
- Rezultati testova
- Chat izmedju ucenika
- Da vidi sertifikate

## Ugao gosta (javno)
- O skoli
- **Bivsi ucenici (gde su danas)**
- Uspesi ucenika
- Da vidim spisak nastavnika (o svima)

### Roditelj (je i gost)
- Online placanje
- Finansije
- Upisane godine
- Lista elektronskih sertifikata
- Komentari nastavnika (odgovor)
- Kad su konsultacije

### Nastavnici
- Da kace obavestenja
- Da kace materijale
- Posalju poruku roditeljima
- Posalju poruku ucenicima
- Vide kontakte ucenika i roditelja
- Evidencija ocena/poena
- Termini i grupe
- Konsultacije
- 

### Upis u skolu
- Elektronska prijava
- Opis testa
- Obavestenja povodom testa
- Objavljeni rezultati

### Upis godine
- Azuziranje stanja ucenika u bazi
- Azuriranje 

## Administrator
- Finansije
- Sve vidi

### Pravljenje dijagram slucaja upotrebe
- Prijava za upis
- Obrada prijava (na osnovu svih prijavljenih generisati spisak...)
- Objavljivanje rezultata
- Upis godine
- Dobijanje referentnih informacija za tekucu godinu
    - Objavljivanje materijala
- Finansije
- Opste informacije
    - Pretraga ucenika
    - Pretraga nastavnika

