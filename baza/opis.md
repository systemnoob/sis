## Entiteti

###### Nezavisni entiteti:
- Ucenik
- Roditelj
- Nastavnik
- Godina
- Grupa
- Rata
- Predmet
- Materijal

###### Zavisni entiteti:
- Uplatnica
- Stipendija

###### Agregirani entiteti:
- Roditelj_Ucenik
- Predaje

### Ucenik
Svaki ucenik ima svoj nalog. Kada ucenik zavrsi poslednju godinu, grupa mu se stavlja na NULL. Ucenik se ostavlja u bazi da bi skola eventualno mogla da mu salje informacije u buducnosti o partnerskim firmama, ponudama za posao, itd...
- ime
- prezime
- id_grupe: za ucenika se pamti grupa u kojoj slusa predavanja ili NULL ako je tek upisao ili vec zavrsio
- korisnicko_ime: korisnicko ime kojim ucenik pristupa sistemu
- sifra: hesirana lozinka

### Roditelj
Roditelj ima uvid u finansije ali ne moze da vidi informacije o projektima i ostale stvari koje su potrebne uceniku. Nastavnici mogu da im se obracaju, zakazuju roditeljske sastanke, itd...
- ime
- prezime
- korisnicko_ime: korisnicko ime kojim roditelj pristupa sistemu
- sifra: hesirana lozinka

### Nastavnik
Svaki nastavnik drzi nastavu celoj grupi jedne godine (svaki predmet), i moze da predaje vise grupa. Takodje moze da bude mentor na projektima.
- ime
- prezime
- korisnicko_ime: korisnicko ime kojim nastavnik pristupa sistemu
- sifra: hesirana lozinka

### Godina
Sadrzi informacije o rednom broju godine i o ceni / ceni za talentovane.
- godina: redni broj godine (1,2,3,...)
- cena: redovna cena godine
- cena_za_talentovane: cena godine sa popustom za talentovanu decu

### Grupa
Grupu pohadja odredjeni broj ucenika na jednoj godini.
Po prelasku u drugu godinu, menja se id_godine grupe ucenika. Grupa se moze izabrati da ima isto obelezje kao prethodna, ali ukoliko ucenik ima razlog, sistem dopusta i da ne bude ista.
- id_godine: godina koju grupa pohadja
- obelezje: neki "naziv" grupe ("A,B,C")

### Rata
Sadrzi informacije o iznosu koji treba da se uplati, kao i za kog ucenika u kojoj godini se placa. Sistem generise 4 jednake mesecne rate (okidac)
- iznos: uplacena kolicina novca
- id_ucenika: ucenik za koga je izvrsena uplata
- id_godine: godina koju ucenik pohadja (je pohadjao)
- rb_rate: (1, 2, 3, 4)

### Predmet
Sadrzi informacije o nazivu predmeta i na kojoj se godini slusa. Na godini moze biti vise predmeta
- naziv
- id_godine

### Materijal
Sadrzi informacije o nazivu materijala, kao i o njegovom autoru i linku sa kog se moze preuzeti. Jedan predmet moze da ima vise materijala.
- naziv
- autor
- url
- id_predmeta

### Uplatnica
Sadrzi informacije o uplacenoj rati
- id_rate: rata za koju je vezana uplatnica.
- datum_uplate
- id_roditelja: roditelj koji je izvrsio uplatu

### Stipendija
Sadrzi informaciju o ucenicima koji su dobro plasirani na prijemnom ispitu (najboljih 30), koji jeftinije placaju skolarinu.
- id_ucenika
- broj_poena_na_testu

### Roditelj_Ucenik
Veza izmedju roditelja i ucenika (otac i majka mogu da poseduju nalog za jednog ucenika, i roditelj moze imati vise dece).
- id_ucenika
- id_roditelja

### Predaje
Veza izmedju nastavnika i ucenika.
- id_grupe
- id_nastavnika
- broj_ucionice
