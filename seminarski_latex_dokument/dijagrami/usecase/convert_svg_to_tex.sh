#! /bin/sh
# Use visual paradigm to extract diagrams as 'svg' and then run this
# script to convert them to pdf.
# We go this way because it's much easier to include pdf then svg in LaTeX.
# Make sure to have inkscape installed before running the script.

# Kill file extension
for f in *.svg; do
    fname=$f
    fname=${fname%.*}

    svg_img="$fname.svg"
    pdf_img="$fname.pdf"

    echo "Exporting $fname as pdf..."
    inkscape -D -z --file="$svg_img" --export-pdf="$pdf_img"
    echo "done!"
done

